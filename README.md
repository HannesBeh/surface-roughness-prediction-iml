# Surface Roughness Prediction IML

### Description
This project contains several modelling and feature extraction approaches for predicting the surface roughness in a robot-based incremental manufacturing milling setup.

### data
>- Vibrational time series data as well as process parameters and surface roughness as target variable
>- Data processing steps for vibrational data

### feature_engineering
>- PCA and tsfresh for creating feature_set1 and feature_set2

### modeling
>- Different prediction models for predicting surface roughness based on three different feature sets

### process_optimization
>- Process optimization using correlation analysis as well as graphical visualizations
